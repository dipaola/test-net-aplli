<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->string('matricule',10);
            $table->string('nom',25);
            $table->string('prenoms',50)->nullable();
            $table->string('adresse1',25);
            $table->string('adresse2',25)->nullable();
            $table->integer('code_postal')->nullable();
            $table->string('ville',25);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
};
