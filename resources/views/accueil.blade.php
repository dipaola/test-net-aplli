<html>
    <head>
        <title>Import excel</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    </head>
    <body>
            <div class='container'>
                <div class='navbar'>

                </div>

                <div class="row">
                    <div class="col-12 m-3">
                        
                            <div class='card card-primary'>
                                <div class='card-header'>
                                    importation de données CSV
                                </div>
                                <div class='card-body'>

                                    <form  class="form form-horizontal" action="{{ route('enregistrer') }}" method="POST" enctype="multipart/form-data">
                                       @csrf 
                                        <input class='form-control' name='fichier' type='file' placeholder='cliquez ici pour importer le fichier' required/>
                                        <br>
                                        <input type="submit" class="btn btn-primary" name="importer le fichier" value='Importer le fichier'/>
                                        <a href="{{ route('liste') }}" class= "btn btn-outline-success">Afficher la liste</a>

                                    </form>


                                </div>
                            </div>


                    </div>
                </div>
            </div>
    </body>
</html>
