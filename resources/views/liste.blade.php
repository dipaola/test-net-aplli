<html>
    <head>
        <title>Liste des employés</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    
    </head>
    <body>
            <div class='container'>
                <div class='navbar'>

                </div>

                <div class="row">
                    <div class="col-12 m-3">
                        
                            <div class='card card-primary'>
                                <div class='card-header'>
                                    Liste des employes
                                </div>
                                <div class='card-body'>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>N°</th>
                                                <th>Matricule</th>
                                                <th>Nom & prenoms</th>
                                                <th>Adresses</th>
                                                <th>Code Postal</th>
                                                <th>Ville</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($employes as $employe)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $employe->matricule }}</td>
                                                <td>{{ $employe->nom.' '.$employe->prenoms }}</td>
                                                <td>{{ $employe->adresse1 }}
                                                    @isset($employe->adresse2) {{ ' / '.$employe->adresse2}} @endisset
                                                </td>
                                                <td>{{ $employe->code_postal }}</td>
                                                <td>{{ $employe->ville }}</td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan='6'>Rien à afficher</td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>

                                    <br>
                                    <a href="{{ route('accueil') }}" class= "btn btn-success">retour à l'accueil</a>

                                </div>
                            </div>


                    </div>
                </div>
            </div>
    </body>
</html>
