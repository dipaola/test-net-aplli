<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home',[EmployeeController::class , 'index'])->name('accueil');
Route::post('/enregistrer/employee',[EmployeeController::class , 'store'])->name('enregistrer');
Route::get('/liste',[EmployeeController::class , 'afficher'])->name('liste');