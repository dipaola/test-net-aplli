<?php

namespace App\Imports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class EmployeeImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        Validator::make($rows->toArray(), [
            '*.0' => 'required|string|max:10',
            '*.1' => 'required|string|max:25',
            '*.2' => 'nullable|string|max:50',
            '*.3' => 'required|string|max:25',
            '*.4' => 'nullable|string|max:25',
            '*.5' => 'nullable|integer|max:10',
            '*.6' => 'required|string|max:25',
        ])->validate();


        $tableau = [];

        foreach ($rows as $row)
        {
           $tableau[] = array([
            'matricule' => $row[0],
            'nom' => $row[1],
            'prenoms' => $row[2],
            'adresse1' => $row[3],
            'adresse2' => $row[4],
            'code_postal' => $row[5],
            'ville' => $row[6],
            ]);
        }

        return $tableau;
    }

    
}
