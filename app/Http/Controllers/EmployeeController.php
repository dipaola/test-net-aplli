<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use App\Imports\EmployeeImport;
use App\Http\Requests\EmployeeRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view ('accueil');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function afficher()
    {
        $listeEmployes = Employee::liste();
       return view('liste')->with(['employes' =>$listeEmployes, 'i' => 1 ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
       
       $employes = Excel::toArray(new EmployeeImport, $request->file('fichier') );
       
       DB::beginTransaction();

       foreach($employes[0] as $unite){
          
        if($unite[0] != 'MATRICULE'){
             Employee::create([
               'matricule' => $unite[0],
               'nom' => $unite[1],
               'prenoms' => is_null($unite[2]) ? Null : $unite[2],
               'adresse1' => $unite[3],
               'adresse2' => is_null($unite[4]) ? Null : $unite[4],
               'code_postal' => is_null ($unite[5]) ? Null : (int) $unite[5],
               'ville' => $unite[6],
           ]);
        }
          
       }

       DB::commit();
       
       $listeEmployes = Employee::liste();
       
       return view('liste')->with(['employes' =>$listeEmployes, 'i' => 1 ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
