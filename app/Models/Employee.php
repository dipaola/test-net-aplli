<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $table ='employees';
    protected $fillable = [
        'matricule',
        'nom',
        'prenoms',
        'adresse1',
        'adresse2',
        'code_postal',
        'ville'
    ];


    public static function liste() {
        return Employee::all();
    }

}
